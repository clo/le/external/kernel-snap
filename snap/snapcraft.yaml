name: qti-sa8155p-kernel
summary: Snapdragon kernel
description: This is a snapdragon snapped kernel, based off linaro kernel
grade: stable
confinement: strict
type: kernel
adopt-info: kernel

architectures:
  - build-on: [amd64, arm64]
    run-on: [arm64]

build-base: core20

parts:
  kernel:
    after:
      - firmware
    plugin: x-kernel
    source: https://git.launchpad.net/~ubuntu-cervinia/+git/linux-kernel
    kconfigflavour: "iot"
    source-type: git
    source-depth: 1
    source-branch: dev-5.15
    kernel-with-firmware: false
    kernel-enable-zfs-support: true
    kernel-initrd-compression: lz4
    kernel-initrd-modules:
      - atl1c
      - qcom-emac
      - phy-qcom-qusb2
      - slimbus
      - xhci-hcd
      - libarc4
    kconfigs:
      - CONFIG_DEBUG_INFO=n
      - CONFIG_SYSTEM_TRUSTED_KEYS=""
      - CONFIG_SYSTEM_REVOCATION_KEYS=""
    override-build: |
      snapcraftctl build
      snapcraftctl set-version $(dpkg-parsechangelog -l ${SNAPCRAFT_PART_SRC}/debian.iot/changelog -S version)
    override-prime: |
      snapcraftctl prime
      ${SNAPCRAFT_STAGE}/trim-firmware ${SNAPCRAFT_PRIME}
    stage:
      - dtbs
      - System.map-*
      - config-*
      - initrd.img
      - kernel.img
      - modules
    prime:
      - -dtbs
      - -initrd.img
      - -kernel.img

  bootimg:
    plugin: nil
    after:
      - kernel
    override-build: |
      cat ${SNAPCRAFT_STAGE}/kernel.img \
          ${SNAPCRAFT_STAGE}/dtbs/qcom/sa8155p-adp.dtb \
          ${SNAPCRAFT_STAGE}/dtbs/qcom/sa8155p-iot-v2-adp.dtb \
          > Image.gz+dtbs
      mkbootimg \
        --kernel Image.gz+dtbs \
        --ramdisk ${SNAPCRAFT_STAGE}/initrd.img \
        --output ${SNAPCRAFT_PART_INSTALL}/boot.img.nonsecure \
        --pagesize 4096 \
        --base 0x80000000 \
        --kernel_offset 0x8000 \
        --ramdisk_offset 0x1000000 \
        --tags_offset 0x100 \
        --cmdline 'console=tty0 console=ttyMSM0,115200n8 clk_ignore_unused pd_ignore_unused'
    prime:
      - boot.img

  firmware:
    plugin: dump
    source: https://git.launchpad.net/~canonical-kernel-snaps/+git/kernel-snaps-uc22
    source-type: git
    source-branch: main
    stage-packages:
      - linux-firmware
      - wireless-regdb
    organize:
      lib/firmware: firmware
    prime:
      - firmware

  firmware-custom:
    after:
      - kernel
    plugin: nil
    override-pull: |
      # allow custom source definition
      if [ -n "${SNAPDRAGON_KERNEL_SNAP_FIRMWARE:-}" ]; then
        if [ -d ${SNAPDRAGON_KERNEL_SNAP_FIRMWARE} ]; then
          cp -r ${SNAPDRAGON_KERNEL_SNAP_FIRMWARE}/* ${SNAPCRAFT_PART_SRC}
        else
          git clone --depth 1 ${SNAPDRAGON_KERNEL_SNAP_FIRMWARE} ${SNAPCRAFT_PART_SRC}
        fi
      else
        echo "Missing env SNAPDRAGON_KERNEL_SNAP_FIRMWARE no custom firmware will be included"
        touch no-snapdragon-kernel-snap-firmware
      fi
    override-build: |
      mkdir -p ${SNAPCRAFT_PART_INSTALL}/firmware/qcom/sa8155p
      cp -r * ${SNAPCRAFT_PART_INSTALL}/firmware/qcom/sa8155p

  signing-test-keys:
    plugin: dump
    source: https://git.launchpad.net/~ubuntu-cervinia/+git/cervinia-test-keys
    source-type: git
    organize:
      '*': signing-keys/
    prime:
      - -*

  sign-bootimg:
    plugin: nil
    after:
      - bootimg
      - signing-test-keys
    override-build: |
      KEY_NAME="bootimg-key"
      BOOT_IMG="boot.img"
      TARGET_SHA_TYPE=sha256
      BOARD_KERNEL_PAGESIZE=2048
      # create signature block
      openssl \
        dgst \
        -${TARGET_SHA_TYPE} \
        -binary ${SNAPCRAFT_STAGE}/${BOOT_IMG}.nonsecure \
        > ${BOOT_IMG}.${TARGET_SHA_TYPE}
      openssl \
        pkeyutl \
        -sign \
        -in ${BOOT_IMG}.${TARGET_SHA_TYPE} \
        -inkey ${SNAPCRAFT_STAGE}/signing-keys/${KEY_NAME}.key \
        -out ${BOOT_IMG}.sig \
        -pkeyopt digest:sha256 \
        -pkeyopt rsa_padding_mode:pkcs1

      # append it to the image
      dd if=/dev/zero of=${BOOT_IMG}.sig.padded bs=${BOARD_KERNEL_PAGESIZE} count=1
      dd if=${BOOT_IMG}.sig of=${BOOT_IMG}.sig.padded conv=notrunc
      cat ${SNAPCRAFT_STAGE}/${BOOT_IMG}.nonsecure ${BOOT_IMG}.sig.padded > ${SNAPCRAFT_PART_INSTALL}/${BOOT_IMG}

build-packages:
    - dpkg-dev
    - android-tools-mkbootimg
    - libssl-dev
    - libfdt-dev
    - cpio
    - flex
    - bison
    - wget
    - gcc-aarch64-linux-gnu
    - linux-libc-dev

package-repositories:
    - type: apt
      ppa: snappy-dev/image
